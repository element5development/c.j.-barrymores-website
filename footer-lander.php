<div class="clearfix"></div>
<footer class="lander-footer">
  <div class="container">
    <div class="row">
      © Copyright 2017 C.J. Barrymore’s
    </div>
  </div>
</footer>
<?php wp_footer(); ?>
<!--[if lt IE 7 ]>
<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
<![endif]-->



<!-- JS -->
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/classie.js"></script>
<!--<script type="text/javascript" src="<?php //echo get_template_directory_uri();  ?>/js/uisearch.js"></script>-->
<!--<script>
        new UISearch( document.getElementById( 'sb-search' ) );
</script>-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.simpleWeather.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/simpleWeather.functions.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.sidr.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/functions.js"></script>
<!--<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>-->
<script src="<?php echo get_template_directory_uri(); ?>/library/js/slick.min.js"></script>
<script>
  $(document).ready(function(){
    //slick slider script
    $('.test-slider').slick({
      autoplay: true,
      autoplaySpeed: 4000,
      arrows: true,
      nextArrow: '<a class="next-arrow"><img src="<?php echo get_template_directory_uri(); ?>/images/next.png" alt="next arrow"></a>',
      prevArrow: '<a class="prev-arrow"><img src="<?php echo get_template_directory_uri(); ?>/images/prev.png" alt="prev arrow"></a>'
    });

    $('#book-now-btn').on('click', function(){
      $('#top-buttons').hide();
      $('.book-now-contain').toggleClass('hidden');
    });

  });
</script>

<!--[if lt IE 9]>
<script src="js/selectivizr-min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/eCSStender.js"></script>
<![endif]-->
</body>

</html>