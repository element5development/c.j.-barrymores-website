// Docs at http://simpleweatherjs.com
    jQuery(document).ready(function() {
    $.simpleWeather({
    woeid: '2381790', //2381790 Clinton Township MI 2399226
    //Palm Beach, FL 2467663
    //Anchorage, AK 2354490
    //Clinton Township, MI 2381790
    //Marquette 2446014
    location: '',
    unit: 'f',
    success: function(weather) {
      html = '<div class="icon icon-'+weather.code+'"><\/div>';
      html += '<div class="temp">'+weather.temp+'&deg;F<\/div>';
      // html += '<div>'+weather.city+', '+weather.region+'</div>';
      // html += '<div class="currently">'+weather.currently+'</div>';
      //html += '<li>'+weather.tempAlt+'&deg;C</li></ul>';
      

      // Weather Codes

      // 0 tornado
      // 1 tropical storm
      // 2 hurricane
      // 3 severe thunderstorms
      // 4 thunderstorms
      // 5 mixed rain and snow
      // 6 mixed rain and sleet
      // 7 mixed snow and sleet
      // 8 freezing drizzle
      // 9 drizzle
      // 10  freezing rain
      // 11  showers
      // 12  showers
      // 13  snow flurries
      // 14  light snow showers
      // 15  blowing snow
      // 16  snow
      // 17  hail
      // 18  sleet
      // 19  dust
      // 20  foggy
      // 21  haze
      // 22  smoky
      // 23  blustery
      // 24  windy
      // 25  cold
      // 26  cloudy
      // 27  mostly cloudy (night)
      // 28  mostly cloudy (day)
      // 29  partly cloudy (night)
      // 30  partly cloudy (day)
      // 31  clear (night)
      // 32  sunny
      // 33  fair (night)
      // 34  fair (day)
      // 35  mixed rain and hail
      // 36  hot
      // 37  isolated thunderstorms
      // 38  scattered thunderstorms
      // 39  scattered thunderstorms
      // 40  scattered showers
      // 41  heavy snow
      // 42  scattered snow showers
      // 43  heavy snow
      // 44  partly cloudy
      // 45  thundershowers
      // 46  snow showers
      // 47  isolated thundershowers
      // 3200  not available


      $("#weather").html(html);
	   $("#weather-mobile").html(html);

        //Display Outdoor (71+ Degree Summer Weather No Rain or Severe Weather)
        if (weather.temp >= 71 && weather.code != 0 && weather.code != 3 && weather.code != 4 && weather.code != 5 && weather.code != 6 && weather.code != 8 && weather.code != 9 && weather.code != 10 && weather.code != 11 && weather.code != 12 && weather.code != 17 && weather.code != 18 && weather.code != 35 && weather.code != 37 && weather.code != 38 && weather.code != 39 && weather.code != 40 && weather.code != 45 && weather.code != 47){
           $(".activities.outdoor").show();
           $("#outdoor").show(); //Show Content
           $("#videocontents").load('/wp-content/themes/barrymore/videos/video1.html'); //Load Video
        }

        //Display Indoor/Outdoor (41 - 70 Degrees No Rain or Severe Weather)
        else if (weather.temp <= 70 && weather.temp >= 41 && weather.code != 0 && weather.code != 3 && weather.code != 4 && weather.code != 5 && weather.code != 6 && weather.code != 8 && weather.code != 9 && weather.code != 10 && weather.code != 11 && weather.code != 12 && weather.code != 17 && weather.code != 18 && weather.code != 35 && weather.code != 37 && weather.code != 38 && weather.code != 39 && weather.code != 40 && weather.code != 45 && weather.code != 47){
             $(".activities.both").show();
             $("#both").show(); //Show Content
             $("#videocontents").load('/wp-content/themes/barrymore/videos/video2.html'); //Load Video
        }
        
        //Display Indoor (40 and Below and/or Rain/Severe Weather)
        else {
            $(".activities.indoor").show();
            $("#indoor").show(); //Show Content
            $("#videocontents").load('/wp-content/themes/barrymore/videos/video3.html'); //Load Video
        }

    },
    error: function(error) {
      $("#weather").html('<p>'+error+'<\/p>');
	  $("#weather-mobile").html('<p>'+error+'<\/p>');
    }
    });
    });
