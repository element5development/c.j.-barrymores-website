<?php
/*
Template Name: Private Parties Page
*/
?>

<?php get_header(); ?>

<!---container-->
<div class="container wide">
	<div class="page-heading">
		<div class="page-headertop">
			<h1>
				<?php the_title(); ?>
			</h1>
			<?php if(function_exists('rdfa_breadcrumb')){ rdfa_breadcrumb(); } ?>
		</div>
	</div>

	<div class="event-menu">
		<?php wp_nav_menu( array('menu' => 'event-menu' )); ?>
	</div>


	<!--***************************************************************************************-->

	<div class="party-paks">
		<h2>
			<?php the_field('pak_cards_header'); ?>
		</h2>
		<p class="party-paks-p">
			<?php the_field('pak_cards_description'); ?>
		</p>
		<?php if ( have_rows('pak_cards') ) : ?>
		<div class="party-cards">
			<?php while( have_rows('pak_cards') ) : the_row(); ?>
			<div class="party-card">
				<?php $bgimage = get_sub_field('background_image'); ?>
				<div class="title" style="background-image: url(<?php echo $bgimage['url']; ?>);">
					<div class="block">
						<h3>
							<?php the_sub_field('title'); ?>
						</h3>
						<p class="subheader">
							<?php the_sub_field('subheader'); ?>
						</p>
					</div>
					<div class="overlay"></div>
					<?php if ( get_sub_field('special_value') ) { ?>
					<div class="special">
						<p>
							<?php the_sub_field('special_value'); ?>
							<span>
								<?php the_sub_field('special_message'); ?>
							</span>
						</p>
					</div>
					<?php } ?>
				</div>
				<div class="price">
					<p>
						<?php echo '$'; the_sub_field('price'); ?>
					</p>
				</div>
				<div class="description">
					<p>
						<b>PAK INCLUDES:</b>
					</p>
					<ul>
						<li class="has-icon">
							<svg height="23" viewBox="0 0 23 23" width="23">
								<path d="M7.9565 12c-1.0849.0335-1.9721.462-2.6619 1.2857h-1.346c-.549 0-1.0111-.1356-1.3861-.4068S2 12.211 2 11.6886c0-2.3638.4152-3.5457 1.2455-3.5457.0402 0 .1859.0703.437.2109s.5775.283.9793.4269c.4018.144.8003.216 1.1953.216.4487 0 .894-.077 1.336-.231-.0335.2477-.0502.4687-.0502.6629 0 .9308.2712 1.788.8136 2.5714zm10.7578 6.3984c0 .8036-.2444 1.438-.7333 1.9035-.4888.4654-1.1384.6981-1.9486.6981h-8.779c-.8103 0-1.4599-.2327-1.9487-.6981-.4889-.4654-.7333-1.0999-.7333-1.9035 0-.3549.0117-.7014.0352-1.0396.0234-.3382.0703-.7031.1406-1.0948.0703-.3918.159-.755.2662-1.0899a5.1634 5.1634 0 0 1 .432-.9793c.1807-.3181.3883-.5893.6227-.8137.2344-.2243.5206-.4034.8588-.5373.3382-.134.7115-.201 1.12-.201.067 0 .211.072.4319.216.221.144.4654.3047.7333.4822.2678.1774.626.3381 1.0747.4821.4487.144.9007.216 1.356.216s.9074-.072 1.356-.216c.4487-.144.807-.3047 1.0749-.4821.2678-.1775.5122-.3382.7332-.4822.221-.144.365-.216.432-.216.4084 0 .7817.067 1.12.201.338.1339.6244.313.8587.5373.2344.2244.442.4956.6228.8137.1808.318.3248.6445.432.9793.107.3348.1958.6981.2661 1.0899.0703.3917.1172.7566.1406 1.0948.0235.3382.0352.6847.0352 1.0396zM8.4286 5.5714c0 .7099-.2511 1.3159-.7534 1.8181-.5022.5022-1.1082.7534-1.818.7534-.7099 0-1.3159-.2512-1.8181-.7534-.5023-.5022-.7534-1.1082-.7534-1.818s.2511-1.316.7534-1.8182C4.5413 3.2511 5.1473 3 5.857 3c.7099 0 1.3159.2511 1.8181.7533.5023.5023.7534 1.1083.7534 1.8181zM15.5 9.4286c0 1.0647-.3767 1.9737-1.13 2.727-.7534.7534-1.6624 1.1301-2.7271 1.1301-1.0648 0-1.9738-.3767-2.7272-1.13-.7533-.7534-1.13-1.6624-1.13-2.7271s.3767-1.9738 1.13-2.7271 1.6624-1.13 2.7272-1.13c1.0647 0 1.9737.3766 2.727 1.13.7534.7533 1.1301 1.6623 1.1301 2.727zm5.7857 2.26c0 .5223-.1875.9191-.5625 1.1903-.375.2712-.837.4068-1.3861.4068h-1.346c-.6898-.8237-1.577-1.2522-2.6619-1.2857.5425-.7835.8137-1.6406.8137-2.5714 0-.1942-.0168-.4152-.0503-.663.442.154.8873.231 1.336.231.395 0 .7935-.072 1.1953-.216.4018-.1439.7282-.2862.9793-.4268.2512-.1406.3968-.211.437-.211.8303 0 1.2455 1.182 1.2455 3.5458zM20 5.5714c0 .7099-.2511 1.3159-.7533 1.8181-.5023.5022-1.1083.7534-1.8181.7534s-1.3159-.2512-1.8181-.7534-.7534-1.1082-.7534-1.818.2512-1.316.7534-1.8182S16.7187 3 17.4285 3s1.316.2511 1.8182.7533c.5022.5023.7533 1.1083.7533 1.8181z"
									fill-rule="evenodd" />
							</svg>
							<b>
								<?php the_sub_field('guest_total'); ?> Guests</b> Total (Adults & Kids)
						</li>
						<li class="has-icon">
							<svg height="23" viewBox="0 0 23 23" width="23" xmlns="http://www.w3.org/2000/svg">
								<path d="M20.625 3c.5156 0 .957.1836 1.3242.5508.3672.3672.5508.8086.5508 1.3242v14.25c0 .5156-.1836.957-.5508 1.3242-.3672.3672-.8086.5508-1.3242.5508H1.875c-.5156 0-.957-.1836-1.3242-.5508C.1836 20.082 0 19.6406 0 19.125V4.875c0-.5156.1836-.957.5508-1.3242C.918 3.1836 1.3594 3 1.875 3zM1.875 4.5c-.1016 0-.1895.0371-.2637.1113-.0742.0742-.1113.1621-.1113.2637V7.5H21V4.875c0-.1016-.0371-.1895-.1113-.2637-.0742-.0742-.1621-.1113-.2637-.1113zm18.75 15c.1016 0 .1895-.0371.2637-.1113.0742-.0742.1113-.1621.1113-.2637V12H1.5v7.125c0 .1016.0371.1895.1113.2637.0742.0742.1621.1113.2637.1113zM3 18v-1.5h3V18zm4.5 0v-1.5H12V18z"
									fill-rule="evenodd" />
							</svg>
							<b>
								<?php echo '$'; the_sub_field('dollar_in_cards'); ?>
							</b> in Fun Cards
						</li>
					</ul>
					<?php if ( get_sub_field('description') ) : ?>
					<?php the_sub_field('description'); ?>
					<?php endif; ?>
				</div>
				<?php if ( get_sub_field('button') ) : $link = get_sub_field('button'); ?>
				<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
					<?php echo $link['title']; ?>
				</a>
				<?php endif; ?>
			</div>
			<?php endwhile; ?>
		</div>
		<?php endif; ?>
	</div>
	<!-- <div class="pak-recommendation">
		<?php if ( get_the_content() ) : ?>
		<div class="contents">
			<?php the_content(); ?>
		</div>
		<?php endif; ?>
		<div class="form-contain">
			<p>HAVING TROUBLE DECIDING WHICH PACKAGE FITS YOU BEST?</p>
			<h2>LET US HELP YOU PLAN</h2>
			<?php echo do_shortcode('[gravityform id="9" title="false" description="false" ajax=true]'); ?>
		</div>
	</div> -->
	<div class="additional-party-info">
		<h2>
			<?php the_field('additional_party_info_header'); ?>
		</h2>
		<h3>
			<?php the_field('row_1_header'); ?>
		</h3>
		<div class="row-grid">
			<div class="grid-block">
				<?php if ( get_field('row_1_left') ) : ?>
				<?php the_field('row_1_left'); ?>
				<?php endif; ?>
			</div>
			<div class="grid-block">
				<div class="wristbands">
					<p>UNLIMITED 3 HOUR WRISTBANDS</p>
					<p style="text-align: center;"><strong>$30.00 PER PERSON</strong></p>
					<p>Minimum purchase of 35 wristbands required<br>(excludes Arcade & Batting Cages)</p>
				</div>
				<div class="fun-cards">
					<p>FUN CARDS</p>
					<?php if ( get_field('row_1_right') ) : ?>
					<?php the_field('row_1_right'); ?>
					<?php endif; ?>
					<p>(Bonus Cash not good for food & beverage)</p>
				</div>
			</div>
		</div>
		<h3>
			<?php the_field('row_2_header'); ?>
		</h3>
		<div class="row-grid">
			<div class="grid-block">
				<?php if ( get_field('row_2_left') ) : ?>
				<?php the_field('row_2_left'); ?>
				<?php endif; ?>
			</div>
			<div class="grid-block">
				<?php if ( get_field('row_2_right') ) : ?>
				<?php the_field('row_2_right'); ?>
				<?php endif; ?>
			</div>
		</div>
		<?php if ( get_field('brochure') ) : $link = get_field('brochure'); ?>
		<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
			<?php echo $link['title']; ?>
		</a>
		<?php endif; ?>
	</div>


	<!--***************************************************************************************-->

	<!-- party package -->
	<div class="party-package">
		<?php echo do_shortcode('[types field="party-package"][/types]') ?>
	</div>
	<!-- end party package -->
</div>
<!-- end #container -->

<!-- invitations -->
<div class="invitations">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<?php echo do_shortcode('[types field="ticket-worth"][/types]') ?>
			</div>
		</div>
	</div>
</div>
<!-- end invitations -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript" src="http://cjbarrymores.com/wp-content/themes/barrymore/js/jquery.arctext.js"></script>
<script>
	jQuery('#example1').arctext({
		radius: 40
	});
</script>
<?php get_footer(); ?>