<?php
/*
Template Name: Events Landing
*/
?>

<?php get_header('lander'); ?>

<div style="background-image: url('<?php the_field('lander_banner_image'); ?>')" class="lander-banner-contain">
  <div class="container">

    <?php if( get_field('lander_banner_headline') ): ?>
      <div class="row">
        <div class="col-sm-12 col-md-6 col-md-offset-3 lander-headline-contain">
          <?php the_field('lander_banner_headline'); ?>
        </div>
      </div>
    <?php endif; ?>

    <?php if( get_field('lander_dates') ): ?>
      <div class="row">
        <div class="col-sm-12 col-md-6 col-md-offset-3 lander-header-date-contain">
          <h2><?php the_field('lander_dates'); ?></h2>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>
<div class="clearfix"></div>

<div class="newsletter-contain">
  <div class="email-icon">
    <img src="<?php echo get_template_directory_uri(); ?>/images/envelope.png" alt="envolope">
  </div>
  <div class="container">
    <?php if( get_field('lander_newsletter_headline') ): ?>
      <div class="row newsletter-headline">
        <h2><?php the_field('lander_newsletter_headline'); ?></h2>
      </div>
    <?php endif; ?>

    <?php if( get_field('lander_newsletter_form') ): ?>
      <div class="row newsletter-form">
        <div class="col-sm-12 col-md-8 col-md-offset-2">
          <?php the_field('lander_newsletter_form'); ?>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>

<?php if( get_field('promo_area_headline') ): ?>
  <div class="promo-area-contain" style="background-image: url('<?php the_field('promotion_area_background_image'); ?>')">
    <div class="container">
      <div class="row promo-headline-contain">
        <h2><?php the_field('promo_area_headline'); ?></h2>
      </div>

    <?php if( get_field('promotion_area_upper_text') ): ?>
      <div class="row upper-promo-contain">
        <?php the_field('promotion_area_upper_text'); ?>
      </div>
    <?php endif; ?>

    <?php if( get_field('promo_box_rate') ): ?>
      <div class="row">
        <div class="promo-box-contain col-sm-12 col-md-6 col-md-offset-3">
          <div class="row">

            <div class="rate-contain col-sm-12 col-md-6">
              <h3>$<?php the_field('promo_box_rate'); ?></h3>
            </div>

            <?php if( get_field('promo_box_right_text') ): ?>
              <div class="right-text col-sm-12 col-md-6">
                <?php the_field('promo_box_right_text'); ?>
              </div>
            <?php endif; ?>
          </div>
          <?php if( get_field('promo_box_lower_text') ): ?>
            <div class="row promo-box-lower">
              <?php the_field('promo_box_lower_text'); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    <?php endif; ?>

    <?php if( get_field('lower_promo_area_text') ): ?>
      <div class="row lower-promo-area-contain">
        <?php the_field('lower_promo_area_text'); ?>
      </div>
    <?php endif; ?>
      </div>
    </div>
  </div>
<?php endif; ?>

<?php if( get_field('lander_social_buttons') ): ?>
  <div class="container">
    <div class="row social-buttons-contain">
      <?php if (have_rows('lander_social_buttons')):
        while (have_rows('lander_social_buttons')) : the_row();
          if (get_sub_field('social_button_type') == 'Facebook') :
            echo '<a href="https://www.facebook.com/CJBarrymores" target="_blank" class="btn facebook-btn"><img src="'.get_template_directory_uri().'/images/facebook-btn.png" alt="Facebook"></a>';
          endif;
          if (get_sub_field('social_button_type') == 'Twitter'):
            echo '<a href="https://twitter.com/CJBarrymores" target="_blank" class="btn twitter-btn"><img src="'.get_template_directory_uri().'/images/twitter-btn.png" alt="Twitter"></a>';
          endif;
          if (get_sub_field('social_button_type') == 'Instagram'):
            echo '<a href="https://www.instagram.com/cjbarrymores/" target="_blank" class="btn insta-btn"><img src="'.get_template_directory_uri().'/images/insta-btn.png" alt="Instagram"></a>';
          endif;
        endwhile;
      endif ?>
    </div>
  </div>
<?php endif; ?>

<?php if( get_field('lander_add_rides') ): ?>
  <div class="container lander-ride-container">
    <div class="row">
      <h2>The Rides</h2>
    </div>
    <div class="row">
      <?php if(have_rows('lander_add_rides')): ?>
        <?php while(have_rows('lander_add_rides')):  the_row(); ?>
          <div class="lander-ride col-sm-12 col-md-6 col-lg-3">
          <?php if( get_sub_field('ride_link') ): ?>
            <a href="<?php the_sub_field('ride_link'); ?>">
              <div class="lander-ride-image" style="background-image: url('<?php the_sub_field('ride_image'); ?>')"></div>
            </a>
            <?php else: ?>
            <div class="lander-ride-image" style="background-image: url('<?php the_sub_field('ride_image'); ?>')"></div>
          <?php endif; ?>
            <div class="lander-ride-copy">
              <h3><?php the_sub_field('ride_headline'); ?></h3>
              <?php the_sub_field('ride_details'); ?>
            </div>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
<?php endif; ?>

<?php if( get_field('location_section_background_image') ): ?>
  <div class="location-contain" style="background-image: url('<?php the_field('location_section_background_image'); ?>')">
    <div class="row map-headline">
      <h2>Our Location</h2>
    </div>

  <?php if( get_field('location_section_address') ): ?>
    <div class="row location-address">
      <?php the_field('location_section_address'); ?>
    </div>
  <?php endif; ?>

  <?php if( get_field('location_section_contact_info') ): ?>
    <div class="row location-contact">
      <?php the_field('location_section_contact_info'); ?>
    </div>
  <?php endif; ?>
  </div>
<?php endif; ?>

<?php get_footer('lander'); ?>
