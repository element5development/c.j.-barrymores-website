<div class="clearfix"></div>	
<footer role="contentinfo" id="footer">
    <div id="socail-bg">
        <div class="fade-color-cover">
            <div class="container">
                <h2>Follow Us on Social!</h2>
                <?php //echo ot_get_option('social_links'); ?>
                <ul class="list-unstyled">
                    <li><a target="_blank" href="<?php echo ot_get_option('facebook'); ?>" title="Facebook" class="facebook-col social-bg-roll"></a></li>
                    <!-- <li><a target="_blank" href="<?php echo ot_get_option('twitter'); ?>" title="Twitter" class="twitter-col social-bg-roll"></a></li> -->
                    <!-- <li><a target="_blank" href="<?php echo ot_get_option('google_plus'); ?>" title="Google Plus" class="googleplus-col social-bg-roll"></a></li> -->
                    <!-- <li><a target="_blank" href="<?php echo ot_get_option('yelp'); ?>" title="Yelp" class="yelp-col social-bg-roll"></a></li> -->
                    <li><a target="_blank" href="<?php echo ot_get_option('instagram'); ?>" title="Instagram" class="instagram-col social-bg-roll"></a></li>
                    <!-- <li><a target="_blank" href="<?php echo ot_get_option('youtube'); ?>" title="YouTube" class="youtube-col social-bg-roll"></a></li> -->
                    <!-- <li><a target="_blank" href="<?php echo ot_get_option('trip_advisor'); ?>" title="Trip Advisor" class="tri-padvisor-col social-bg-roll"></a></li> -->
                </ul>
            </div><!--container ends-->
            <div class="newsletter-container">
                 <h2>Join Our Newsletter</h2>
                 <?php echo do_shortcode('[gravityform id="6" title="false" description="false"]'); ?>
            </div>
        </div><!--fade-color-cover ends-->
        <div class="clearfix"></div>
    </div><!--socail-bg ends-->


    <div class="container">
        <div id="inner-footer" class="clearfix">

            <div id="widget-footer" class="clearfix row">

<!--<div class="footer1 col-sm-4"> <?php //if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer1') ) : endif;  ?></div>-->
<!--<div class="footer2 col-sm-8"> <?php //if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer2') ) : endif;  ?></div>-->
                <div class="footer1 col-md-3 col-sm-12 col-xs-12">
                    <p>© <?php echo date('Y'); ?> C.J. Barrymore's</p>
                    <div class="clearfix"></div>
                    <nav id="secondary">
                        <?php wp_nav_menu(array('menu' => 'Header Right Menu')); ?>
                    </nav>
                </div>

                <?php echo ot_get_option('footer_setting'); ?>

            </div>

            <nav class="clearfix">
                <?php wp_bootstrap_footer_links(); // Adjust using Menus in Wordpress Admin ?>
            </nav>



        </div> <!-- end #inner-footer -->
    </div>
</footer> <!-- end footer -->



</div>

<?php wp_footer(); ?>


<!--[if lt IE 7 ]>
        <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
        <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
<![endif]-->



<!-- JS -->
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/classie.js"></script>
<!--<script type="text/javascript" src="<?php //echo get_template_directory_uri();  ?>/js/uisearch.js"></script>-->
<!--<script>
        new UISearch( document.getElementById( 'sb-search' ) );
</script>-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.simpleWeather.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/simpleWeather.functions.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.sidr.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/functions.js"></script>

<!--[if lt IE 9]>
        <script src="js/selectivizr-min.js" type="text/javascript"></script>
                <script type="text/javascript" src="js/eCSStender.js"></script>
        <![endif]-->
</body>

</html>