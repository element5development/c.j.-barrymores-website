<!doctype html>

<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><?php wp_title('|', true, 'right'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/jquery.sidr.dark.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/flexslider.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/slick.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/main.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/weather.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
  <!--[if lt IE 9]>
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/ie8-and-down.css" />
  <![endif]-->
  <?php wp_head(); ?>
  <!-- end of wordpress head -->

  <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <!--SLIDER POSTS ENDS-->
  <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-modal.js"></script>

  <!--SLIDER POSTS-->
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/jquery.bxslider.css">
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.min.js"></script>
  <script type="text/javascript">
    var j = jQuery.noConflict();
    j(document).ready(function () {
      j('.bxslider').bxSlider({
        auto: true,
        controls: true,
        autoControls: true
      });
    });
  </script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.js"></script>
  <!-- Hotjar Tracking Code for http://cjbarrymores.com/ -->
  <script>
    (function(h,o,t,j,a,r){
      h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
      h._hjSettings={hjid:486940,hjsv:5};
      a=o.getElementsByTagName('head')[0];
      r=o.createElement('script');r.async=1;
      r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
      a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
  </script>
  <!-- CALLRAIL CALL TRACKING TRACKING -->
  <script type="text/javascript" src="//cdn.callrail.com/companies/906902226/472d9e4b9b1fed37d630/12/swap.js"></script>
	<!-- Snapchat Pixel for http://cjbarrymores.com/ -->
	<script type='text/javascript'>
		(function (win, doc, sdk_url) {
			if (win.snaptr) return;
			var tr = win.snaptr = function () {
				tr.handleRequest ? tr.handleRequest.apply(tr, arguments) : tr.queue.push(arguments);
			};
			tr.queue = [];
			var s = 'script';
			var new_script_section = doc.createElement(s);
			new_script_section.async = !0;
			new_script_section.src = sdk_url;
			var insert_pos = doc.getElementsByTagName(s)[0];
			insert_pos.parentNode.insertBefore(new_script_section, insert_pos);
		})(window, document, 'https://sc-static.net/scevent.min.js');

		snaptr('init', 'c6041540-13c3-40be-825e-0260d4062192', {
			'user_email': '<USER-EMAIL>'
		})
		snaptr('track', 'PAGE_VIEW')
	</script>

</head>

<body <?php body_class(); ?>>
<header role="banner" id="header">
  <div class="top-lander" id="headertop">
    <div class="container">
      <a href="/" id="logo">C.J. Barrymore's</a>
    </div><!--container ends-->
  </div><!--#headertop-->
</header>
<div class="clearfix"></div>