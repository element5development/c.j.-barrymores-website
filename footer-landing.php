<!--BEGIN FOOTER-->
 <footer class="reg_f">
    <div class="container-fluid">
       <div class="row">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="contact_info">
                    <div class="first"><?php echo get_field('address'); ?></div>
                    <ul>
                        <li><a href="tel:<?php echo str_replace('.','',get_field('phone')); ?>">P: <?php echo get_field('phone'); ?></a></li>
                        <li><a href="tel:<?php echo str_replace('.','',get_field('fax')); ?>"> F: <?php echo get_field('fax') ?></a></li>
                        <li><a href="mailto:<?php echo get_field('email'); ?>">E: <?php echo get_field('email'); ?></a></li>
                    </ul>
                    <p class="last"><?php echo get_field('copyright'); ?></p>
                </div>            
             </div>
         </div>  
    </div>    
</footer>
<!--END FOOTER-->


<?php wp_footer(); ?>

 
<!--BEGIN INCLUDE BOOTSTRAP-->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<!--END INCLUDE BOOTSTRAP-->
	
<!--BEGIN INCLUDE MAIN SCRIPT-->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/js.js"></script>
<!--END INCLUDE MAIN SCRIPT-->
  
</body>
</html>