<?php
/*
  Template Name: Homepage
 */
?>

<?php get_header(); ?>

<div class="clearfix"></div>
<div id="reviews-col">
    <div class="container">
        <div id="content" class="clearfix row">
            <div id="main" class="col-sm-12 clearfix" role="main">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                        <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">



                            <section class="row post_content">

                                <div class="col-sm-12">

                                    <?php the_content(); ?>

                                </div>


                            </section> <!-- end article header -->

                            <footer>

                                <p class="clearfix"><?php the_tags('<span class="tags">' . __("Tags", "wpbootstrap") . ': ', ', ', '</span>'); ?></p>

                            </footer> <!-- end article footer -->

                        </article> <!-- end article -->

                        <?php
                        // No comments on homepage
                        //comments_template();
                        ?>

                    <?php endwhile; ?>	

                <?php else : ?>

                    <article id="post-not-found">
                        <header>
                            <h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
                        </header>
                        <section class="post_content">
                            <p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
                        </section>
                        <footer>
                        </footer>
                    </article>

                <?php endif; ?>

            </div> <!-- end #main -->

            <?php //get_sidebar(); // sidebar 1 ?>

        </div> <!-- end #content -->
    </div><!--container ends-->
</div><!--reviews-col ends-->
<div class="clearfix"></div>
<!--Home Column-->
<!---container-->
<div class="container">            
    <div class="row home_column">
    <!--<div class="col-sm-4 home-col-1"><?php //if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('home-col-1') ) : endif;  ?></div>-->
    <!--<div class="col-sm-4 home-col-2"><?php //if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('home-col-2') ) : endif;  ?></div>-->
    <!--<div class="col-sm-4 home-col-3"><?php //if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('home-col-3') ) : endif;  ?></div>-->

        <div class="col-sm-4 info-box-1 hidden-xs hidden-sm">
             	<?php echo ot_get_option('let_us_do_the_work'); ?>
        </div><!--info-box-1 ends-->

        <div class="col-sm-4 info-box-2 hidden-xs hidden-sm">
                <?php echo ot_get_option('view_our_entertainment'); ?>
        </div><!--info-box-2 ends-->

        <div class="col-sm-4 info-box-3 hidden-xs hidden-sm">
                <?php echo ot_get_option('view_hours_and_get_directions'); ?>
        </div><!--info-box-3 ends-->

        <!--For Small Mobile devices content-->
        <div class="col-sm-12 col-xs-12 hidden-md hidden-lg">
            <div class="mobile-content-box tour">
                <?php echo ot_get_option('come_and_visit'); ?>
                
<!--                <h3>COME AND VISIT</h3>
                <p>21750 Hall Road<br> Clinton Township, MI 48038-1541</p>
                <a href="http://c3scj.staging.wpengine.com/hours-directions/" class="link-btn">Get Directions >></a>-->
            </div><!--mobile-content-box ends-->
        </div>

        <div class="col-sm-12 col-xs-12 hidden-md hidden-lg">
            <div class="mobile-content-box">
                <?php echo ot_get_option('contact_us_today'); ?>
<!--                <h3>CONTACT US TODAY</h3>
                <p>P: 586.469.2800 <span>•</span> F: 586.463.4874<br> E: <a href="mailto:mail@cjbarrymores.com">mail@cjbarrymores.com</a></p>
                <a href="#" class="link-btn">Click to Call >></a>-->
            </div><!--mobile-content-box ends-->
        </div>

        <!-- <div class="col-sm-12 col-xs-12 hidden-md hidden-lg">
            <div class="red-info-box">
                <a href="<?php echo site_url('coupons'); ?>">
                    <h4>Get Coupons Now</h4>
                </a>
            </div>red-info-box ends
        </div> -->

        <div class="col-sm-12 col-xs-12 hidden-md hidden-lg">
            <div class="blue-info-box">
                <a href="<?php echo site_url('parties-events'); ?>">
                    <h4>HOST AN AMAZING PARTY</h4>
                </a>
            </div><!--red-info-box ends-->
        </div>

        <div class="col-sm-12 col-xs-12 hidden-md hidden-lg">
            <div class="blue-info-box">
                <a href="<?php echo site_url('hours-directions'); ?>">
                    <h4>VIEW HOURS &amp; GET DIRECTIONS</h4>
                </a>
            </div><!--red-info-box ends-->
        </div>

        <div class="col-sm-12 col-xs-12 hidden-md hidden-lg">
            <div class="blue-info-box">
                <a href="<?php echo site_url('entertainment'); ?>">
                    <h4>View our ENTERTAINMENT!</h4>
                </a>
            </div><!--red-info-box ends-->
        </div>
        <!--For Small Mobile devices content-->
    </div><!--row home_column ends-->

    <!--Home Column Ends-->

</div> <!-- end container -->
<?php get_footer(); ?>