<!doctype html>  

<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php wp_title( '|', true, 'right' ); ?></title>	
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">
				
		<!-- media-queries.js (fallback) -->
		<!--[if lt IE 9]>
			<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>			
		<![endif]-->

		<!-- html5.js -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
  		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
  		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/jquery.sidr.dark.css">
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/main.css">
		<?php wp_head(); ?>
		<!-- end of wordpress head -->
		

<!--SLIDER POSTS ENDS-->
		
		<script src="<?php echo get_template_directory_uri(); ?>/js/openWeather.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-modal.js"></script>
		<script type="text/javascript">
				j('.weather-nav li:last-child').addClass( 'bgnone');
				
			});
		</script>
		<script type="text/javascript">
			$(function () {
				$('.tabs').tabs()
			})
		</script>

<!--SLIDER POSTS-->
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/jquery.bxslider.css">
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.min.js"></script>
	<script type="text/javascript">
	var j = jQuery.noConflict();
	j(document).ready(function(){
	  j('.bxslider').bxSlider({
	  auto: false,
	  autoControls: true
	});
	});
	</script> 

<script>
jQuery(document).ready(function()
{
    var width = jQuery(window).width(); //alert(width);
	j("#mobicon").hide();
    //This is ajax code which will send width to your php page in the screenWidth variable
    j.ajax({
    url: "http://c3scj.wpengine.com/test.php", //this will be your php page 
    data : {screenwidth:width} 
    }).done(function(data) 
	{ 
	 if(data<=767)
	 { 
	    j("#mobicon").show();
		j(".activity").hide();
		j(".todays-hot").hide();
		jQuery(".mobileAct").on('click',function()
		{ 
			jQuery(".activity").slideToggle();
			jQuery(".todays-hot").slideToggle();
			
		 
		});
	
	 }
	 	//jQuery(this).addClass("done");
    });
	
	 if(width <= 768)
	{  
	    jQuery("#mobicon").show();
		jQuery(".activity").hide();
		jQuery(".mobileAct").on('click',function()
		{ 
			jQuery(".activity").slideToggle();
		 
		});
			
	} 
		
});	 
</script>
		<!-- Snapchat Pixel for http://cjbarrymores.com/ -->
		<script type='text/javascript'>
			(function (win, doc, sdk_url) {
				if (win.snaptr) return;
				var tr = win.snaptr = function () {
					tr.handleRequest ? tr.handleRequest.apply(tr, arguments) : tr.queue.push(arguments);
				};
				tr.queue = [];
				var s = 'script';
				var new_script_section = doc.createElement(s);
				new_script_section.async = !0;
				new_script_section.src = sdk_url;
				var insert_pos = doc.getElementsByTagName(s)[0];
				insert_pos.parentNode.insertBefore(new_script_section, insert_pos);
			})(window, document, 'https://sc-static.net/scevent.min.js');

			snaptr('init', 'c6041540-13c3-40be-825e-0260d4062192', {
				'user_email': '<USER-EMAIL>'
			})
			snaptr('track', 'PAGE_VIEW')
		</script>
		

		
	</head>
	
	<body <?php body_class(); ?>>

		<header role="banner" id="header">
		
		<div id="headertop">
			<div class="wrap">
			<a href="<?php echo home_url(); ?>" id="logo">CJ Barrymore's</a>

			<div id="mobile-nav">
                    <a id="responsive-menu-button" href="#sidr-main"><span>Navigation</span></a>
            </div><!-- #mobile-nav-->

            <div id="navigation">
                
                <div id="mobile-click">
                	<a href="#" onclick="jQuery.sidr('close', 'sidr-main');" class="close">Close Menu <span class="red">x</span></a>
                    <h2><a href="tel:1-586-469-2800"><span>Click to call</span>586.469.2800</a></h2>
                </div><!--#click-->

				<nav id="nav" role="navigation">
					<?php wp_nav_menu( array('menu' => 'primary') ); ?>
				</nav>
				<nav id="secondary">
					<?php wp_nav_menu( array('menu' => 'Header Right Menu') ); ?>
				</nav>

			</div><!--#navigation-->

			</div><!--.wrap-->
		</div><!--#headertop-->
		
				<?php
				global $wpdb;
				$city="Clinton Township, MI";
				$country="USA"; //Two digit country code
				$url="http://api.openweathermap.org/data/2.5/weather?q=".$city.",".$country."&units=metric&cnt=7&lang=en";
				$json=file_get_contents($url);
				$data=json_decode($json,true);
				//Get current Temperature in Celsius
				$temp= $data['main']['temp']."<br>";
				//Get weather condition
				$data['weather'][0]['main']."<br>";
				//Get cloud percentage
				$data['clouds']['all']."<br>";
				//Get wind speed
				$data['wind']['speed']."<br>";
				//37°C x  9/5 + 32 = 98.6°F
				$Currenttemp = floor($temp * 9/5 + 32);
				?>
				<div class="hours-f">
					<div class="container">
					<form method="post" name="weather">
					<div class="weather-wrapper">
						<strong><span class="weather-temperature"><?php echo $Currenttemp."°F"; ?></span> </strong>
						<input type="hidden" name="temp" id="temp" class="weather-temperature" />
					</div>	
					</form>
					
				</div>
					
				<div class="tmpClass">
				
					<div class="hours">
					<div class="container">
					
					<ul class="weather-nav">
					<div class="blockbg">
					<li class="weather bgnone"><a href="http://www.weather.com/weather/today/USMI0178:1:US" target="_blank">
					<?php if($Currenttemp < 40 ) {?>
						<img src="<?php bloginfo("stylesheet_directory"); ?>/images/settings.png"/></a>
					<?php }else{ ?>	
						<img src="<?php bloginfo("stylesheet_directory"); ?>/images/sun.png"/></a>
					<?php } ?>		
					</li>
					<li>AT CJ's Right Now</li></div>
					<li class="activity"><strong>Today's hot entertainment</strong></li>
					<?php 
					$tempRange = "SELECT * FROM tbl_temp_range WHERE $Currenttemp BETWEEN range_start AND range_end";
					$qry = $wpdb->get_row($tempRange);
					
								
					$tempSelect = "select * from tbl_temprature where temprature_id='".$qry->id."'";
					$query = $wpdb->get_results($tempSelect);
					foreach($query as $key=>$row)
					{
						$post_id = explode(',', $row->activities);
					} 
								
					    $args = array('post__in' => $post_id ,'posts_per_page' => 4, 'orderby' => 'rand');
						$the_query = new WP_Query( $args ); 
										
						if( $the_query->have_posts() ) :
							while ( $the_query->have_posts() ) : $the_query->the_post();
						    
					?>
						<li class="<?php //echo $class; ?> activity"><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
					<?php endwhile; endif; wp_reset_query(); ?>
						      
					
			</ul>
			<span id="mobicon"><a href="javascript:;" id="mobileCSS" class="mobileAct">icon</a></span>
			<!-- SEARCH BUTTONS -->
				<div class="lg col-sm-4 searchbg">
				<div id="sb-search" class="sb-search">
						<form action="<?php echo home_url( '/' ); ?>" method="get">
						    <input class="sb-search-input" placeholder="Search..." type="search" value="" name="s" id="s" action="<?php echo home_url( '/' ); ?>">
						    <input class="sb-search-submit" type="submit" value="">
						    <span class="sb-icon-search"></span>
						</form>
						</div>		
				
				</div>
			<!-- SEARCH BUTTONS -->
			</div></div> 
			
			<?php if(is_front_page()){  ?>
			<?php if($Currenttemp > 1 && $Currenttemp < 41 ) {  ?> 
			
				<div class="home_slider">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('home-videos1') || !dynamic_sidebar('home_slider')) : endif; ?>
				</div>
			
			<?php } else if($Currenttemp > 41 && $Currenttemp < 71) {   ?>
			 <div class="home_slider">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('home-videos2') || !dynamic_sidebar('home_slider1') ) : endif; ?>
			</div>
			<?php } else if($Currenttemp > 71) {  ?>
			<div class="home_slider">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('home-videos3') || !dynamic_sidebar('home_slider2')) : endif; ?>
			</div>
			
			<?php } }else{  ?>
			 <div class="inner-slider"><?php echo do_shortcode('[types field="inner-slider"][/types]') ?> </div> 
			<?php } ?> 
		            
			</div>
			
				
		
		</header> <!-- end header -->
		 
		 
