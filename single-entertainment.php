<?php
/*
Template Name: single entertainment
*/
?>
<?php get_header(); ?>
		
		<!--<div class="inner-slider"><img src="<?php //bloginfo("stylesheet_directory"); ?>/images/slider-inner.jpg"/>
			</div>-->	
			
			<!---container-->
			<div class="container">
					
				
			<div id="content" class="clearfix row">
			
				<div id="main" class="col-sm-12 postinfo clearfix" role="main">
			
				<ul> 
			
<?php 
/* $postId = $post->ID; 

$category = get_the_category( $postId );

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
echo $catname = $category[0]->cat_name;

$new_query = new WP_Query();
$new_query->query('post_id='.$postId.'&category_name='.$catname.'&showposts=1'.'&paged='.$paged); */
$post = $wp_query->post;
$postId = $post->ID; 
$category = get_the_category( $postId );
$paged = (get_query_var('page')) ? get_query_var('page') : 1;
$catname = $category[0]->cat_name;

//$catname = "All Entertainment";

$catid = $category[0]->term_id;
$Subcatid  = $category[1]->term_id;
//$catid =9;
//'post_id='.$postId.'&
$wp_query = new WP_Query();
$wp_query->query('p='.$postId.'&cat='.$catid.'&showposts=1');

while($wp_query->have_posts()) : $wp_query->the_post();
?>
	<li>
	
			<div class="page-heading">
			<div class="page-headertop entertainment">
				<h1><?php the_title(); ?></h1>
				<?php if(function_exists('rdfa_breadcrumb')){ rdfa_breadcrumb(); } ?>
			</div>
			</div>
			
			<div class="row entertainment">
			<?php  $image1 = (types_render_field("upload-single-image", array("raw"=>"true"))); 
			 $image2 = (types_render_field("upload-multiple-image-2", array("raw"=>"true")));
			 $image3 = (types_render_field("upload-multiple-image-3", array("raw"=>"true")));
			 $image4 = (types_render_field("upload-multiple-image-4", array("raw"=>"true")));
			 $image5 = (types_render_field("upload-multiple-image-5", array("raw"=>"true")));
		
	        ?>
			 
			
			<div class="col-sm-8 left-img">
			<div class="upload-single-image">
<ul class="bxslider">
     <?php
	 /*** To Check if image field is empty or not **/
	  if($image1!=''){
	  ?>
		<li><?php echo do_shortcode('[types field="upload-single-image"][/types]') ?> </li>
	  <?php } if($image2!=''){ ?>
	 	<li><?php echo do_shortcode('[types field="upload-multiple-image-2"][/types]') ?> </li>
	   <?php } if($image3!=''){ ?>	
		<li><?php echo do_shortcode('[types field="upload-multiple-image-3"][/types]') ?> </li>
	   <?php }  if($image4!=''){ ?>
	   <li><?php echo do_shortcode('[types field="upload-multiple-image-4"][/types]') ?> </li>
	   <?php }  if($image5!=''){ ?>
	    <li><?php echo do_shortcode('[types field="upload-multiple-image-5"][/types]') ?> </li>
		 <?php } ?>
 </ul>
 
			</div>
			</div>
					
			
			<div class="col-sm-4 right-content">
			
			 <h3 class="page-title"><?php echo do_shortcode('[types field="title"][/types]') ?> </h3>
			<div class="description"><?php echo do_shortcode('[types field="description"][/types]') ?> </div>
			<div class="price-leftimg">
			<h6 class="price-title"><?php echo do_shortcode('[types field="price-title"][/types]') ?> </h6>
			<p class="price-description"><?php echo do_shortcode('[types field="price-description"][/types]') ?> </p></div>
				
			<div class="requirements-leftimg">
			<h6 class="requirements"><?php echo do_shortcode('[types field="requirements"][/types]') ?> </h6> 
			<p class="requirements-description"><?php echo do_shortcode('[types field="requirements-description"][/types]') ?> </p></div>
			<div class="upload-park-map">
			<h6>View Park Map</h6>
			<a href="https://cjbarrymores.com/wp-content/uploads/2018/03/ParkMap-Jan2018-7.pdf">Click here to view map.</a>
				<!--<?php echo do_shortcode('[types field="upload-park-map"][/types]') ?>-->
			</div>
			
			
			
			</div>
			
			
			
			</div> 
	 
<?php the_content(); ?>
</li>

<li>
<div class="showpostContainer">
<span  class="showPosts"><a href="javascript:;" class="showhide"> show </a></span></div>
	<div class="allPosts">
	<h3 class="select_heading">Select an attraction below</h3>
	<ul class="row">
			<?php
				//$args = array( 'category' => $catid, 'post_type' =>  'post' );
				$args = array( 'posts_per_page' => -1, 'post_type' =>  'post' ); 
				$postslist = get_posts( $args );    
				foreach ($postslist as $post) :  setup_postdata($post); 
				?>  
				<li class="col-sm-3 post-bot"><h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2> </li>
			   
				<?php endforeach; ?> 
				</ul>
			</div>

</li> 
<?php endwhile; ?>
<?php
wp_reset_postdata();
				global $wpdb;
				$args = array(
					'category ' => $catid,
					'order' => 'DESC',
					'posts_per_page'=>1,
					'paged' => $paged

					); 
										
					query_posts($args);
					$total_posts = (int) $wp_query->found_posts;
					$prev_post = get_previous_post(); 
					$prevID = $prev_post->ID ;
					$next_post = get_next_post();
					$nextID = $next_post->ID ;
					$all_posts = $all_post_query->post_count;
					?>
                    <div class="paginate">					
								<?php $prev_post = get_previous_post();
								if (!empty( $prev_post ))
												
								{
								if(!empty($nextID)) { ?><?php  echo '<span class="glyphicon-chevron-right">'; next_post('%', 'Next', 'no'); echo '</span>'; ?><?php } else {?><a class="next"><?php echo '<span class="glyphicon-chevron-left"></span>'; } ; ?></a>
																 
							
								<?php  echo '<span class="glyphicon-chevron-left">'; previous_post('%',' Previous', 'no'); echo '</span>'; ?>  <?php } else { ?><?php  echo '<span class="glyphicon-chevron-left"></span>';  } ; ?>
									
					</div>			
					</div>
				<?php  
				 //$wp_query->max_num_pages; ?>
		
<?php //next_posts_link('&laquo; Next', $new_query->max_num_pages) ?>
<?php //previous_posts_link('Previous &raquo;') ?>
</ul>	
			

<div class="suggested"> 
			<h3 class="single-title" itemprop="headline">Like the <?php the_title(); ?>? <span>Here are some Other awesome suggested cj’s activities.</span></h3></div>
			<ul class="pleft">			
			<?php
			/* echo $postId; 
			foreach(get_the_category($postId) as $cat) 
			{ 
			   if(!$cat->parent) : //check if top cat
			   echo $cat->term_id.' '.$cat->name.' (top cat)<br/>'; //top cat
			   $sub_cats = get_categories('parent='.$cat->term_id);
			   if($sub_cats) foreach($sub_cats as $sub_cat) 
			   {
					echo $sub_cat->term_id.' '.$sub_cat->name.'<br/>'; //sub cat(s)
			   }
			   endif;
			}  */
			
				
				$categories = get_the_category($post->ID);
				if ($categories) {
				$category_ids = array();
				foreach($categories as $individual_category) 
				$category_ids = $individual_category->term_id;

				$args=array(
				'category__in' => $category_ids,
				'post__not_in' => array($post->ID),
				'posts_per_page'=> 5, // Number of related posts that will be shown.
				'caller_get_posts'=>1,
				'orderby' => rand
				);

				$my_query = new wp_query( $args );
				if( $my_query->have_posts() ) 
				{
							
					while( $my_query->have_posts() ) 
					{
						$my_query->the_post();?>

						
							<li> <a href=" <?php echo get_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
						
					<?
					}
						
				}
				}
				
				wp_reset_query();
			 			
			?>

			</ul>
			
			
					
				
			
				</div> <!-- end #main -->
    
			   
			</div> <!-- end #content -->
			</div> <!-- end #container -->	 
<script type="text/javascript">
jQuery(document).ready(function(){
 var content = jQuery(".allPosts").hide();
  jQuery(".showhide").on("click", function(e){
    
    jQuery(".allPosts").slideToggle();
  });
  
    
  var pagerIcons =  jQuery('.bx-pager').children().length; 
  if(pagerIcons==1)
  {
	jQuery('.bx-pager').css('display','none');
  
  }
  
  //alert(pagerIcons);
  
});

</script>			
<?php get_footer(); ?>