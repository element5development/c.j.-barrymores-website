<!doctype html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <title><?php wp_title('|', true, 'right'); ?></title>


    <!-- Bootstrap -->
    <link type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <link type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" />
	  
	  
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon">
    
	<?php wp_head(); ?>
   
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<body <?php body_class(); ?>>


<!--BEGIN HEADER-->
 <header class="reg_h">
    <div class="container-fluid">
       <div class="row">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <a target="_blank" href="https://cjbarrymores.com/"><img src="<?php echo get_field('logo'); ?>" alt="logo"></a>        
             </div>                                       
         </div>  	
    </div>
</header>
<!--END HEADER-->