<?php
/*
Template Name: Entertainment
*/
?>

<?php get_header(); ?>
		
			
			<!---container--->
			<div class="container">
							
				
			<div id="content" class="clearfix row">
			
				<div id="main" class="col-sm-12 clearfix" role="main">
			
				<ul> 
				
				
<?php 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$catname = "Entertainment";
$new_query = new WP_Query();
$new_query->query('category_name='.$catname.'&showposts=1'.'&paged='.$paged);

//$recent = new WP_Query("cat=8&showposts=1&paged='". $paged."'"); 
while($new_query->have_posts()) : $new_query->the_post();


?>

	<li>
	
	<div class="page-heading">
			<div class="page-headertop">
			 <h2 class="page-name"><?php echo do_shortcode('[types field="name"][/types]') ?> </h2> 
			<? //php if(function_exists('rdfa_breadcrumb')){ rdfa_breadcrumb(); } ?>
			</div>
			</div>	
			
			<div class="row entertainment">
			
			<div class="col-sm-8 left-img">
			<div class="upload-single-image">
			
	<ul class="bxslider">
		<li><?php echo do_shortcode('[types field="upload-single-image"][/types]') ?> </li>
		<li><?php echo do_shortcode('[types field="upload-multiple-image-2"][/types]') ?> </li>
		<li><?php echo do_shortcode('[types field="upload-multiple-image-3"][/types]') ?> </li>
 </ul>
			
			</div>
			</div>
			
		
			
			
			
			<div class="col-sm-4 right-content">
			 <h3 class="page-title"><?php echo do_shortcode('[types field="title"][/types]') ?> </h3>
			<div class="description"><?php echo do_shortcode('[types field="description"][/types]') ?> </div>
			<h6 class="price-title"><?php echo do_shortcode('[types field="price-title"][/types]') ?> </h6>
			<p class="price-description"><?php echo do_shortcode('[types field="price-description"][/types]') ?> </p>
			<h6 class="requirements"><?php echo do_shortcode('[types field="requirements"][/types]') ?> </h6> 
			<p class="requirements-description"><?php echo do_shortcode('[types field="requirements-description"][/types]') ?> </p>
			<div class="upload-park-map"><?php echo do_shortcode('[types field="upload-park-map"][/types]') ?> </div>
			</div>
			
			</div>
			
<?php //next_posts_link(); ?>
<?php //previous_posts_link(); ?>

			 
<?php the_content(); ?>
</li>

<li>
<div class="showpostContainer">
<span class="showPosts"><a href="javascript:;" class="showhide"> show </a></span>
	<div class="allPosts">
			<?php
				$args = array( 'category' => 8, 'post_type' =>  'post' ); 
				$postslist = get_posts( $args );    
				foreach ($postslist as $post) :  setup_postdata($post); 
				?>  
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2> 
			   
				<?php endforeach; ?> 
			</div>
</div>
</li>

<?php endwhile; ?>


		
<?php next_posts_link('&laquo; Next', $new_query->max_num_pages) ?>
<?php previous_posts_link('Previous &raquo;') ?>
</ul>	
					
					
				
			
				</div> <!-- end #main -->
    
			   
			</div> <!-- end #content -->
			</div> <!-- end #container -->	 
<script type="text/javascript">
jQuery(document).ready(function(){
 var content = jQuery(".allPosts").hide();
  jQuery(".showhide").on("click", function(e){
    
    jQuery(".allPosts").slideToggle();
  });
});

</script>	
<?php get_footer(); ?>