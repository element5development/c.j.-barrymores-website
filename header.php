<!doctype html>

<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!-->
<html <?php language_attributes(); ?> class="no-js">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>
		<?php wp_title('|', true, 'right'); ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/jquery.sidr.dark.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/flexslider.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/slick.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/birthday-template.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/private-parties-template.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/weather.css">
	<!--[if lt IE 9]>
                        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/ie8-and-down.css" />
                <![endif]-->
	<?php wp_head(); ?>
	<!-- end of wordpress head -->

	<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<!--SLIDER POSTS ENDS-->
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-modal.js"></script>
	<!--SLIDER POSTS-->
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/jquery.bxslider.css">
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.min.js"></script>
	<script type="text/javascript">
		var j = jQuery.noConflict();
		j(document).ready(function () {
			j('.bxslider').bxSlider({
				auto: true,
				controls: true,
				autoControls: true
			});
		});
	</script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.js"></script>
	<!-- Hotjar Tracking Code for http://cjbarrymores.com/ -->
	<script>
		(function (h, o, t, j, a, r) {
			h.hj = h.hj || function () {
				(h.hj.q = h.hj.q || []).push(arguments)
			};
			h._hjSettings = {
				hjid: 486940,
				hjsv: 5
			};
			a = o.getElementsByTagName('head')[0];
			r = o.createElement('script');
			r.async = 1;
			r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
			a.appendChild(r);
		})(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');
	</script>
	<!-- Snapchat Pixel for http://cjbarrymores.com/ -->
	<script type='text/javascript'>
		(function (win, doc, sdk_url) {
			if (win.snaptr) return;
			var tr = win.snaptr = function () {
				tr.handleRequest ? tr.handleRequest.apply(tr, arguments) : tr.queue.push(arguments);
			};
			tr.queue = [];
			var s = 'script';
			var new_script_section = doc.createElement(s);
			new_script_section.async = !0;
			new_script_section.src = sdk_url;
			var insert_pos = doc.getElementsByTagName(s)[0];
			insert_pos.parentNode.insertBefore(new_script_section, insert_pos);
		})(window, document, 'https://sc-static.net/scevent.min.js');

		snaptr('init', 'c6041540-13c3-40be-825e-0260d4062192', {
			'user_email': '<USER-EMAIL>'
		})
		snaptr('track', 'PAGE_VIEW')
	</script>

</head>

<body <?php body_class(); ?>>

	<header role="banner" id="header">

		<div id="headertop">
			<div class="black-line-left hidden-xs"></div>
			<!--black-line-left ends-->
			<div class="container">
				<div class="weather-box hidden-xs">
					<div id="weather">
						<img alt="Loading..." class="loading" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/loader.gif">
						<br>Loading...
					</div>
					<!-- #weather -->
					<p>At CJ’s Right Now</p>
				</div>
				<!--weather-box ends-->
				<a href="<?php echo home_url(); ?>" id="logo">CJ Barrymore's</a>
				<nav id="secondary">
					<?php wp_nav_menu(array('menu' => 'Header Right Menu')); ?>
				</nav>
			</div>
			<!--container ends-->
			<div class="black-line-right hidden-xs"></div>
			<!--black-line-right ends-->
		</div>
		<!--#headertop-->

		<div id="new-navigation-bar">
			<div class="container">
				<div class="today-hours">
					<!-- <?php 
				
									 $myDateTime = new DateTime(date("Y-m-d H:i:s"), new DateTimeZone('GMT'));
									 $myDateTime->setTimezone(new DateTimeZone('America/Detroit'));
									 $curdate = $myDateTime->format('Y-m-d');
									 $date = strtotime($curdate);
									 //ini_set( 'date.timezone', 'America/Detroit');    
									 echo '['. $curdate .'] '; 
									 echo todays_events();
									 $matches = array();
									 //echo strip_tags(preg_match('/<span class="event-title"(.+?)>(.+?)</span>/', '', todays_events()), '');
									 preg_match('/\<span class="event-title"(.+?)\>(.+?)\<\/span\>/', todays_events(), $matches);
									 $today_hour =  $matches[2];
									 echo '[['. date('Y-m-d H:i:s', ctwo()) .']]';

								?> -->
					<?php
                       //global $wpdb;								
                       //$results =  $wpdb->get_results("SELECT * FROM wp_calendar WHERE event_event_begin <= '$curdate' AND event_end >= '$curdate';", ARRAY_A);
								
                       $today_hour = 'CLOSED';
								$matches = array();
								
								if (preg_match('/\<span class="event-title"(.+?)\>(.+?)\<\/span\>/', todays_events(), $matches)) {
									$today_hour =  $matches[2];
							   }
								
								?>


						<h2>
							<a href="<?php echo home_url(); ?>/hours-directions/">
								<span>Today’s Hours</span>
								<?php echo $today_hour; ?>
								<br/>
								<span>Golf Dome</span> 9am - 9pm</a>
						</h2>
				</div>
				<!--today-hours ends-->
				<div class="navbar navbar-default">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="menu-down-img">Menu
								<img src="https://cjbarrymores.com/wp-content/themes/barrymore/images/new-mobile-menu-icon.png" alt="">
							</span>
						</button>
					</div>
					<!--navbar-header ends-->
					<div class="navbar-collapse collapse main-menu">
						<ul class="nav navbar-nav">
							<li class="hidden-lg hidden-md hidden-sm">
								<div class="weather-box-mobile">
									<div id="weather-mobile">
										<img alt="Loading..." class="loading" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/loader.gif">
										<br>Loading...
									</div>
									<!-- #weather -->
								</div>
								<!--weather-box ends-->
								<div class="call-box hidden-sm">
									<a href="tel:1-586-469-2800">
										<h2>
											<img src="https://cjbarrymores.com/wp-content/themes/barrymore/images/tele.png" alt="">Click here to call us</h2>
									</a>
								</div>
								<!--call-box ends-->
							</li>


							<?php
                                $defaults = array(
                                  
                                    'menu' => 'primary',
                                   'items_wrap' => '%3$s',
                                    'container' => false,
                                    
                                );

                                wp_nav_menu($defaults);
                                ?>

								<!--                                <li><a href="http://c3scj.staging.wpengine.com/hours-directions/">Hours &amp; Directions</a></li>
                                <li><a href="http://c3scj.staging.wpengine.com/parties-events/">Parties &amp; Events</a></li>
                                <li><a href="http://c3scj.staging.wpengine.com/entertainment/">Entertainment</a></li>
                                <li><a href="http://c3scj.staging.wpengine.com/coupons/">Coupons</a></li>
                                <li><a href="http://c3scj.staging.wpengine.com/park-info/">Park Info</a></li>-->
						</ul>
					</div>
					<!--nav-collapse ends-->
				</div>
				<!--navbar-default ends-->
			</div>
			<!--container ends-->
		</div>
		<!--new-navigation-bar ends-->


		<?php if (is_front_page()) { ?>

		<div class="homecontainer">
			<div class="slider-box">
				<div class="flexslider">
					<ul class="slides">
						<?php
                $sliderQuery = new WP_Query(array(
                  'post_type' => 'slider',
                  'posts_per_page' => 5
                ));
                if($sliderQuery->have_posts()):while($sliderQuery->have_posts()):$sliderQuery->the_post();
                if(has_post_thumbnail()):
                  $thumb_id = get_post_thumbnail_id(get_the_ID());
                ?>
							<li style="background: url(<?php echo wp_get_attachment_url($thumb_id);?>) no-repeat center top; background-size: cover;">
								<div class="container">
									<div class="slider-caption">
										<?php the_content();?>
									</div>
									<!--slider-caption ends-->
								</div>
								<!--container ends-->
							</li>
							<?php endif;endwhile;endif;?>
					</ul>
				</div>
				<!--flexslider ends-->
				<div class="banner-slide-socail-box hidden-xs">
					<ul class="list-unstyled">
						<li>
							<a target="_blank" href="<?php echo ot_get_option('facebook'); ?>" class="fb"></a>
						</li>
						<li>
							<a target="_blank" href="<?php echo ot_get_option('twitter'); ?>" class="tw"></a>
						</li>
						<li>
							<a target="_blank" href="<?php echo ot_get_option('yelp'); ?>" class="ylp"></a>
						</li>
						<div class="clearfix"></div>
					</ul>
				</div>
				<!--slide-socail-box ends-->

				<!--  <div class="container">
                    <div class="slider-caption hidden-lg hidden-md">
                    	<h3>Come and play inside.</h3>
                        <div class="clearfix"></div>
                        <h4>The weather is always perfect and the games never stop.</h4>
                        <div class="clearfix"></div>
                        <a href="http://c3scj.staging.wpengine.com/entertainment/" class="click-more-btn">Click to See our Enterainment</a>
                  </div>
                  </div> -->

			</div>
			<!--slider-box ends-->
		</div>
		<!--homecontainer ends-->

		<?php } else { ?>
		<div class="inner-slider">
			<?php echo do_shortcode('[types field="inner-slider"][/types]') ?> </div>
		<?php } ?>



	</header>
	<!-- end header -->
	<div class="clearfix"></div>