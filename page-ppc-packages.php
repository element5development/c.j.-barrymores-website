<?php
/*
Template Name: PPC Packages
*/
get_header(); ?>

<div class="ppc-packages">

<div class="banner">
	<div class="wrap">
		<h2>PLANNING A CORPORATE PARTY OR EVENT?</h2>
		<p>Show your staff a good time that they’ll never forget. CJ Barrymore’s can host an assortment of corporate events to thank your staffers for their hard work, host a meeting in a new environment or simply take your team building to a different level. Hold your event in our sports bar, bowling alley or private covered patio.  Booking is easy and a request form can be filled out online. Booking is also available in person or over the phone! Check out our packages below for more information.</p>
		<h3>LET CJ BARRYMORE’S DO THE WORK FOR YOU.</h3>
	</div>
</div>

		<!---container-->
			<div class="container">
			<div class="page-heading">
			<div class="page-headertop">
			<h1><?php the_title(); ?></h1>
			<?php if(function_exists('rdfa_breadcrumb')){ rdfa_breadcrumb(); } ?>
			</div>
			</div>
			<div id="content" class="clearfix row">
			
			   <div id="main" class="col-sm-12 clearfix ppc-landing" role="main">

				<div class="left">

					<div class="greatfor graybox">
						<h3>Great For:</h3>
						<ul>
							<li>Company holiday parties</li>
							<li>Team building activities</li>
							<li>Executive meetings</li>
							<li>Corporate events</li>
							<li>Offsite training</li>
						</ul>
						<p><strong>Starting at $33.95</strong> Per Person</p>
					</div><!--.greatfor-->

					<div class="packages graybox">
						<h3>PACKAGES INCLUDE</h3>
						<ul>
							<li>Unlimited ride wristbands</li>
							<li>Indoor and outdoor activities</li>
							<li>Great food selections</li>
							<li>Private room or covered patio options</li>
							<li>Additional party extras available</li>
						</ul>
					</div><!--.packages-->

				</div><!--.left-->

				<div class="right">
					<h3>Request a Party Quote</h3>
					<?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="false"]'); ?>	
				</div><!--.right-->

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						
						<!-- <header>
							
							<div class="page-header"><h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1></div>
						
						</header> end article header -->
					
						<section class="post_content clearfix" itemprop="articleBody">
							<?php the_content(); ?>
					
						</section> <!-- end article section -->
						
						<footer>
			
							<?php the_tags('<p class="tags"><span class="tags-title">' . __("Tags","wpbootstrap") . ':</span> ', ', ', '</p>'); ?>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
					
					
					<?php endwhile; ?>		
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>	
					
				<div class="ppc">

					<article class="wristbands">
						<header>
							<h2>Unlimited Rides Wristband</h2>
							<h3>4 Hours</h3>
						</header>
						<div class="content">
							<?php the_field('unlimited_rides'); ?>
						</div>
					</article>

					<article class="outdoor">
						<header>
							<h2>Outdoor BBQ</h2>
							<h3>2 Hours</h3>
						</header>
						<div class="content">
							<?php the_field('outdoor_bbq'); ?>
						</div>
					</article>

					<div class="left">
					<!--<article class="wristbands">
							<header>
								<h2>Unlimited Rides Wristband</h2>
								<h3>4 Hours</h3>
							</header>
							<div class="content">
								<?php the_field('unlimited_rides'); ?>
							</div>
						</article>-->

						<article class="patios">
							<header>
								<h2>Private Covered Patios</h2>
							</header>
							<div class="content">
								<?php the_field('private_covered_patios'); ?>
							</div>
						</article>
					</div><!--.left-->

					<div class="right">

						<article class="extras">
							<header>
								<h2>Party Extras</h2>
							</header>
							<div class="content">
								<?php the_field('party_extras'); ?>
							</div>
						</article>

					</div><!--.right-->
				</div><!--.ppc-->
			
				</div> <!-- end #main -->

			</div> <!-- end #content -->
			</div> <!-- end container -->

	</div><!--ppc-packages-->
<?php get_footer(); ?>