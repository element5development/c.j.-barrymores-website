<ul class="activities indoor">
<?php
// Create the array
$links = array();

$links[0] = '<li><a href="/bumper-cars/">Bumper Cars</a></li>';
$links[1] = '<li><a href="/laser-tag/">Laser Tag</a></li>';
$links[2] = '<li><a href="/bowling/">Bowling</a></li>';
$links[3] = '<li><a href="/restaurant/">Restaurant</a></li>';
$links[4] = '<li><a href="/mini-bowling/">Mini Bowling</a></li>';
$links[5] = '<li><a href="/golf-dome/">Golf Dome</a></li>';
$links[6] = '<li><a href="/laser-tag/">Laser Tag</a></li>';

// Shuffle the array
shuffle($links);

// Display: Links
for ($i = 0; $i < 4; $i++){
   echo $links[$i];
}
?>
</ul>