<ul class="activities both">
<?php
// Create the array
$links = array();

$links[0] = '<li><a href="/indy-track/">Indy Karts</a></li>';
$links[1] = '<li><a href="/grand-prix-track/">Grand Prix</a></li>';
$links[2] = '<li><a href="/bumper-boats/">Bumper Boats</a></li>';
$links[3] = '<li><a href="/rookie-karts/">Rookie Karts</a></li>';
$links[4] = '<li><a href="/turbo-track/">Turbo Karts</a></li>';
$links[5] = '<li><a href="/water-wars/">Water Wars</a></li>';
$links[6] = '<li><a href="/euro-bungy/">Euro Bungy</a></li>';
$links[7] = '<li><a href="/driving-range/">Driving Range</a></li>';
$links[8] = '<li><a href="/miniature-golf/">Miniature Golf</a></li>';
$links[9] = '<li><a href="/rock-climbing/">Rock Climbing</a></li>';
$links[10] = '<li><a href="/bumper-cars/">Bumper Cars</a></li>';
$links[11] = '<li><a href="/laser-tag/">Laser Tag</a></li>';
$links[12] = '<li><a href="/bowling/">Bowling</a></li>';
$links[13] = '<li><a href="/restaurant/">Restaurant</a></li>';
$links[14] = '<li><a href="/mini-bowling/">Mini Bowling</a></li>';
$links[15] = '<li><a href="/golf-dome/">Golf Dome</a></li>';
$links[16] = '<li><a href="/laser-tag/">Laser Tag</a></li>';

// Shuffle the array
shuffle($links);

// Display: Links
for ($i = 0; $i < 4; $i++){
   echo $links[$i];
}
?>
</ul>