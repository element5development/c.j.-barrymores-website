<?php
/*
Template Name: Field Trips
*/
?>

<?php get_header(); ?>
			<!---container-->
			<div class="container">
			<div class="page-heading">
			<div class="page-headertop">
			<h1><?php the_title(); ?></h1>
			<?php if(function_exists('rdfa_breadcrumb')){ rdfa_breadcrumb(); } ?>
			</div>
			</div>
			
			<div class="event-menu"><?php wp_nav_menu( array('menu' => 'event-menu' )); ?></div>
			<div id="content" class="clearfix row">
			
				<div id="main" class="col col-lg-12 clearfix" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<!--<header>
							
							<div class="page-header"><h1><?php the_title(); ?></h1></div>
						
						</header>  end article header -->
					
						<section class="post_content">
							<?php the_content(); ?>
					
						</section> <!-- end article section -->
						
						<footer>
			
							<p class="clearfix"><?php the_tags('<span class="tags">' . __("Tags","wpbootstrap") . ': ', ', ', '</span>'); ?></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
				
					
					<?php endwhile; ?>	
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				</div> <!-- end #main -->
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
			</div> <!-- end #content -->

			<div class="party-package">
				<h3><?php the_field('section_title'); ?></h3>
				<p><?php the_field('section_description'); ?></p>
				<div class="contact_info">
					<img class="alignleft size-full wp-image-65" src="https://cjbarrymores.com/wp-content/uploads/2014/06/call.png" alt="call">
					<?php $phonelink = get_field('phone_number'); ?>
					<?php if( $phonelink ): ?>
						<a href="<?php echo $phonelink['url']; ?>" target="<?php echo $phonelink['target']; ?>"><?php echo $phonelink['title']; ?></a>
					<?php endif; ?>
				</div>
				<div class="contact_info">
					<img class="alignleft size-full wp-image-66" src="https://cjbarrymores.com/wp-content/uploads/2014/06/mail.png" alt="mail">
					<?php $link = get_field('email'); ?>
					<?php if( $link ): ?>
						<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
					<?php endif; ?>
				</div>
			</div>

		</div><!-- end #container -->
		
		<!-- invitations -->
		<div class="invitations"><div class="container">
		<div class="row">
		<div class="col-sm-12"><?php echo do_shortcode('[types field="ticket-worth"][/types]') ?></div>
		</div></div></div>
		<!-- end invitations -->
<?php get_footer(); ?>