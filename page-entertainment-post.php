<?php
/*
Template Name: Entertainment Post Show Page
*/
?>

<?php get_header(); ?>
			<!---container-->
			<div class="container">
			<div class="page-heading">
			<div class="page-headertop">
			<h1><?php the_title(); ?></h1>
			<?php if(function_exists('rdfa_breadcrumb')){ rdfa_breadcrumb(); } ?>
			</div>
			</div>
			
			<div id="content" class="clearfix row">
			
				<div id="main" class="col col-lg-12 clearfix" role="main">


    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active first-child"><a href="#all-entertainment" data-toggle="tab">All Entertainment</a></li>
        <li class="indoor"><a href="#indoor-ntertainment" data-toggle="tab">Indoor Entertainment</a></li>
        <li class="outdoor"><a href="#outdoor-ntertainment" data-toggle="tab">Outdoor Entertainment</a></li>

    </ul>
    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="all-entertainment">
           
				<ul class="entertainment-post">
<?php $recent = new WP_Query("cat=9&showposts=100&orderby=title&order=ASC"); while($recent->have_posts()) : $recent->the_post();?>

 <li>
	 <a href="<?php the_permalink() ?>" rel="bookmark">
	 <div class="row entertenment">
	 <div class="col-xs-10"><div class="thum"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_post_thumbnail( array(150,150) ); ?></a></div>
	 <h6><a href="<?php the_permalink() ?>" rel="bookmark">
	 <?php the_title(); ?>
	 </a>
	 <div class="requirements-post"><?php echo do_shortcode('[types field="requirements-post"][/types]') ?></div>
	</div>
	<div class="col-xs-2"><div class="view-btn">View DETAILS</div></div></a></div> 
	 <?php the_excerpt(); ?> 
	</a>
</li>

  <?php endwhile; ?>
</ul>
        </div>
        <div class="tab-pane" id="indoor-ntertainment">
            
<ul class="entertainment-post">
<?php $recent = new WP_Query("cat=11&showposts=100&orderby=title&order=ASC"); while($recent->have_posts()) : $recent->the_post();?>

 <li>
	<a href="<?php the_permalink() ?>" rel="bookmark">
	<div class="row entertenment">
	<div class="col-xs-10">
	<div class="thum"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_post_thumbnail( array(150,150) ); ?></a></div>
	<h6><a href="<?php the_permalink() ?>" rel="bookmark">
	<?php the_title(); ?>
	</a></h6> <div class="requirements-post"><?php echo do_shortcode('[types field="requirements-post"][/types]') ?></div>
	</div>
	<div class="col-xs-2"><div class="view-btn">View DETAILS</div></div></div>
	<?php the_excerpt(); ?>
	</a>
</li>

  <?php endwhile; ?>
</ul>
        </div>
        <div class="tab-pane" id="outdoor-ntertainment">
           
				<ul class="entertainment-post">
<?php $recent = new WP_Query("cat=12&showposts=100&orderby=title&order=ASC"); while($recent->have_posts()) : $recent->the_post();?>

 <li>
	 <a href="<?php the_permalink() ?>" rel="bookmark">
	 <div class="row entertenment">
	 <div class="col-xs-10"><div class="thum"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_post_thumbnail('entertainment_overview_thumb'); ?></a></div>
	 <h6><a href="<?php the_permalink() ?>" rel="bookmark">
	<?php the_title(); ?>
	</a></h6> <div class="requirements-post"><?php echo do_shortcode('[types field="requirements-post"][/types]') ?></div>
	</div>
	<div class="col-xs-2"><div class="view-btn">View DETAILS</div></div></div>
	<?php the_excerpt(); ?>
	</a>
</li>
  <?php endwhile; ?>
</ul>
        </div></div>

			

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<!--<header>
							
							<div class="page-header"><h1><?php the_title(); ?></h1></div>
						
						</header>  end article header -->
					
						<section class="post_content">
							<?php the_content(); ?>
					
						</section> <!-- end article section -->
						
						<footer>
			
							<p class="clearfix"><?php the_tags('<span class="tags">' . __("Tags","wpbootstrap") . ': ', ', ', '</span>'); ?></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
					
					<?php comments_template(); ?>
					
					<?php endwhile; ?>	
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
			</div> <!-- end #main -->
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
			</div> <!-- end #content -->
			</div> <!-- end container -->

<?php get_footer(); ?>